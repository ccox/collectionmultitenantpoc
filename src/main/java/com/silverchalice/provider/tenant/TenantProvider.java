package com.silverchalice.provider.tenant;

import java.util.Date;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/3/13
 * Time: 8:24 AM
 */
//@Component("tenantProvider")
public class TenantProvider {

    protected Random tenantRandomizer =  null;
    protected String lastTenantId = null;

    public TenantProvider() {
        System.out.println("-----> TenantProvider created.");
    }

    public void initAuthentication() {
        tenantRandomizer =  new Random(new Date().getTime());
        rotateTenant();
    }

    public void unInitAuthentication() {
        tenantRandomizer = null;
        lastTenantId = null;
    }

    public String rotateTenant() {
        if (tenantRandomizer == null) {
            return null;
        }

        char letter = (char) (tenantRandomizer.nextInt(5) + 97);
        lastTenantId = Character.toString(letter);

        return lastTenantId;
    }

    public String getTenantId() {
        return lastTenantId;
    }
}
