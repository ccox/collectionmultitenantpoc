package com.silverchalice.models.article;

import com.silverchalice.models.DateAuditedDocument;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/2/13
 * Time: 6:27 PM
 */
@Document(collection = "#{@tenantProvider.getTenantId()}_articles")
public class Article extends DateAuditedDocument {

    protected List<String> authors;
    protected String content;
    protected Source source;

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> aAuthors) {
        authors = aAuthors;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String aContent) {
        content = aContent;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source aSource) {
        source = aSource;
    }
}
