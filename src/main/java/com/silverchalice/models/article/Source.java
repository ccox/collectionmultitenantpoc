package com.silverchalice.models.article;

import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/2/13
 * Time: 6:29 PM
 */
public class Source {

    @Indexed
    protected String sourceId;
    protected String sourceUrl;

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String aSourceId) {
        sourceId = aSourceId;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String aSourceUrl) {
        sourceUrl = aSourceUrl;
    }
}
