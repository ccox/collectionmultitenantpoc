package com.silverchalice.models.media;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/2/13
 * Time: 6:21 PM
 */
public class Size {

    protected int width;
    protected int height;

    public int getWidth() {
        return width;
    }

    public void setWidth(int aWidth) {
        width = aWidth;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int aHeight) {
        height = aHeight;
    }
}
