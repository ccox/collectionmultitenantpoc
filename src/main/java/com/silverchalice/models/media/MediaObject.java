package com.silverchalice.models.media;

import com.silverchalice.models.DateAuditedDocument;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/2/13
 * Time: 6:17 PM
 */
@Document(collection = "#{@tenantProvider.getTenantId()}_media")
public abstract class MediaObject extends DateAuditedDocument {

    protected String name;
    protected String url;

    public String getName() {
        return name;
    }

    public void setName(String aName) {
        name = aName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String aUrl) {
        url = aUrl;
    }
}
