package com.silverchalice.models.media;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/2/13
 * Time: 6:19 PM
 */
public class AudioObject extends MediaObject {

    protected String encoding;
    protected long length;

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String aEncoding) {
        encoding = aEncoding;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long aLength) {
        length = aLength;
    }
}
