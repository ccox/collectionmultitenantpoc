package com.silverchalice.models;

import org.springframework.data.mongodb.core.index.Indexed;

import javax.persistence.PrePersist;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/2/13
 * Time: 5:17 PM
 */
public abstract class DateAuditedDocument extends AbstractIdDocument {

    protected Date createDate;
    @Indexed
    protected Date lastModifiedDate;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date aCreateDate) {
        createDate = aCreateDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date aLastModifiedDate) {
        lastModifiedDate = aLastModifiedDate;
    }

    @PrePersist
    public void initDates() {
        Date now = new Date();

        setLastModifiedDate(now);

        if (getCreateDate() == null) {
            setCreateDate(now);
        }
    }
}
