package com.silverchalice.mongo.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.util.ReflectionUtils;

import javax.persistence.PrePersist;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA. User: ccox Date: 4/17/13 Time: 3:52 PM
 * 
 * @author cholm
 */
public class PrePersistMongoEventListener extends AbstractMongoEventListener<Object> {
    private static Logger logger = LoggerFactory.getLogger(PrePersistMongoEventListener.class);

    @Override
    public void onBeforeConvert(final Object source) {
        super.onBeforeConvert(source);

        ReflectionUtils.doWithMethods(source.getClass(), new ReflectionUtils.MethodCallback() {
            @Override
            public void doWith(final Method aMethod) throws IllegalArgumentException, IllegalAccessException {
                if (aMethod.isAnnotationPresent(PrePersist.class)) {
                    try {
                        aMethod.invoke(source);
                    } catch (final InvocationTargetException e) {
                        logger.error("Unable to invoke PrePersist method " + aMethod.getName() + " on object "
                                + source.getClass().getName()
                                + ". PerPersist methods must be public and take no arguments.", e);
                    }
                }
            }
        });
    }
}
