package com.silverchalice.config;

import com.mongodb.Mongo;
import com.mongodb.WriteConcern;
import cz.jirutka.mongo.embed.EmbeddedMongoFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/3/13
 * Time: 9:39 AM
 */
@Configuration
@Profile("dev")
@PropertySource("classpath:com/silverchalice/config/mongo-dev.properties")
public class MongoDevConfig extends MongoConfig {
    @Override
    @Bean
    public Mongo mongo() throws Exception {
        EmbeddedMongoFactoryBean factory = new EmbeddedMongoFactoryBean();
        Integer port = Integer.parseInt(env.getProperty("databasePort"));
        factory.setPort(port);
        factory.setVersion("2.4.1");
        factory.setBindIp("localhost");
        Mongo mongo = factory.getObject();
        mongo.setWriteConcern(WriteConcern.SAFE);
        return mongo;
    }
}
