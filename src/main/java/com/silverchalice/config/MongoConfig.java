package com.silverchalice.config;

import com.silverchalice.models.AbstractIdDocument;
import com.silverchalice.mongo.listener.PrePersistMongoEventListener;
import com.silverchalice.provider.tenant.TenantProvider;
import com.silverchalice.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/3/13
 * Time: 9:35 AM
 */
@Configuration
@ComponentScan(basePackages = {"com.silverchalice"})
@EnableMongoRepositories(basePackageClasses = { ArticleRepository.class })
public abstract class MongoConfig extends AbstractMongoConfiguration {

    @Autowired
    Environment env;

    @Override
    protected String getDatabaseName() {
        return env.getProperty("databaseName");
    }

    @Override
    @Bean
    @DependsOn("tenantProvider")
    public MongoTemplate mongoTemplate() throws Exception {
        // This is where you can ensure indexes that are not easily indexed by annotations.
        // mongoTemplate.indexOps(B.class).ensureIndex(new Index().on("a", Order.ASCENDING))
        final MongoTemplate template = super.mongoTemplate();
        return template;
    }

    @Override
    protected String getMappingBasePackage() {
        return AbstractIdDocument.class.getPackage().getName();
    }

    @Bean
    public PrePersistMongoEventListener prePersistMongoEventListener() {
        return new PrePersistMongoEventListener();
    }

    @Bean
    public TenantProvider tenantProvider() {
        return new TenantProvider();
    }
}
