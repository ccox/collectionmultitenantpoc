package com.silverchalice.config;

import com.mongodb.Mongo;
import com.mongodb.WriteConcern;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/3/13
 * Time: 9:38 AM
 */
@Configuration
@Profile("local")
@PropertySource("classpath:com/silverchalice/config/mongo-local.properties")
public class MongoLocalConfig extends MongoConfig {

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        String host = env.getProperty("databaseHost");
        Integer port = Integer.parseInt(env.getProperty("databasePort"));

        Mongo mongo = new Mongo(host, port);
        mongo.setWriteConcern(WriteConcern.SAFE);
        return mongo;
    }
}
