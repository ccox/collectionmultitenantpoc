package com.silverchalice.repository;

import com.silverchalice.models.media.MediaObject;
import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/3/13
 * Time: 9:32 AM
 */
@Repository
public interface MediaRepository extends CrudRepository<MediaObject, ObjectId>{
}
