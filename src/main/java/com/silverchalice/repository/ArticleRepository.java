package com.silverchalice.repository;

import com.silverchalice.models.article.Article;
import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/3/13
 * Time: 9:33 AM
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article, ObjectId>{
}
