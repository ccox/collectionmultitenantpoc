package com.silverchalice;

import com.google.common.collect.Lists;
import com.silverchalice.models.article.Article;
import com.silverchalice.models.article.Source;
import com.silverchalice.models.media.AudioObject;
import com.silverchalice.models.media.MediaObject;
import com.silverchalice.provider.tenant.TenantProvider;
import com.silverchalice.repository.ArticleRepository;
import com.silverchalice.repository.MediaRepository;
import de.svenjacobs.loremipsum.LoremIpsum;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.index.IndexInfo;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@ActiveProfiles(profiles = {"local"})
public class MultiTenantTest {

    @Autowired
    private TenantProvider tenantProvider;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private MongoOperations mongoOps;

    private LoremIpsum loremIpsum= new LoremIpsum();

    private Set<String> collections = null;

    @Before
    public void setup() {
        Assert.assertNotNull(tenantProvider);
        Assert.assertNotNull(articleRepository);
        Assert.assertNotNull(mediaRepository);

        // Simulate a startup environment. Until now, no user has logged into the system yet so we can't locate a tenant.
        tenantProvider.initAuthentication();
    }

    @After
    public void tearDown() {
        tenantProvider.unInitAuthentication();

        // clean up the collections so it won't mess up assertions on the next run
        if (collections != null) {
            for (String collectionName : collections) {
                if (!"system.indexes".equals(collectionName))
                    mongoOps.dropCollection(collectionName);
            }
        }
        collections = null;
    }

    @Test
    public void testMultiTenantSave() {
        String expectedMediaCollectionName = tenantProvider.getTenantId() + ".media";
        AudioObject audioObject = new AudioObject();
        audioObject.setEncoding("MPEG2");
        audioObject.setLength(50000);
        audioObject.setName("AnyName");
        audioObject.setUrl("AnyUrl");
        mediaRepository.save(audioObject);

        // Verify the expected collections (allot for system.indexes)
        collections = mongoOps.getCollectionNames();
        Assert.assertTrue(collections.size() == 2);
        Assert.assertTrue(collections.contains(expectedMediaCollectionName));
        Assert.assertFalse(collections.contains(".media"));

        // Verify there are indexes on the collection
        List<IndexInfo> indexInfo = mongoOps.indexOps(expectedMediaCollectionName).getIndexInfo();
        Assert.assertNotNull(indexInfo);
        Assert.assertTrue(indexInfo.size() == 2);
        for (IndexInfo index : indexInfo) {
            Assert.assertTrue("_id_".equals(index.getName()) || "lastModifiedDate".equals(index.getName()));
        }

        // Verify we can read from the tenant driven collection
        List<MediaObject> mediaObjects = Lists.newArrayList(mediaRepository.findAll());
        Assert.assertNotNull(mediaObjects);
        Assert.assertTrue(mediaObjects.size() == 1);

        tenantProvider.rotateTenant();
        String expectedArticleCollectionName = tenantProvider.getTenantId() +".articles";
        Article article = new Article();
        article.setAuthors(new ArrayList<String>(Arrays.asList("Doe, John")));
        article.setContent(loremIpsum.getParagraphs(5));
        Source source = new Source();
        source.setSourceId("AnyId");
        source.setSourceUrl("AnyUrl");
        article.setSource(source);

        articleRepository.save(article);

        // Verify the expected collections (allot for system.indexes)
        collections = mongoOps.getCollectionNames();
        Assert.assertTrue(collections.size() == 3);
        Assert.assertTrue(collections.contains(expectedMediaCollectionName));
        Assert.assertTrue(collections.contains(expectedArticleCollectionName));
        Assert.assertFalse(collections.contains(".articles"));

        // Verify there are indexes on the collection
        indexInfo = mongoOps.indexOps(Article.class).getIndexInfo();
        Assert.assertNotNull(indexInfo);
        Assert.assertTrue(indexInfo.size() == 2);
        for (IndexInfo index : indexInfo) {
            Assert.assertTrue("_id_".equals(index.getName()) || "lastModifiedDate".equals(index.getName()));
        }

        // Verify we can read from the tenant driven collection
        List<Article> articles = Lists.newArrayList(articleRepository.findAll());
        Assert.assertNotNull(articles);
        Assert.assertTrue(articles.size() == 1);
    }
}
