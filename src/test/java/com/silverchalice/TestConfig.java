package com.silverchalice;

import com.silverchalice.config.MongoConfig;
import com.silverchalice.config.MongoDevConfig;
import com.silverchalice.config.MongoLocalConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created with IntelliJ IDEA.
 * User: ccox
 * Date: 7/5/13
 * Time: 8:37 PM
 */
@Configuration
@Import({MongoConfig.class, MongoDevConfig.class, MongoLocalConfig.class})
public class TestConfig {

}
